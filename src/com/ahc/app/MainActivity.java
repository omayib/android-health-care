package com.ahc.app;

import com.ahc.app.helper.GlobalVar;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class MainActivity extends Activity implements OnClickListener {
	ImageButton bKlink, bRS, bApotik, bTips;
	GlobalVar var;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		var = ((GlobalVar) getApplicationContext());

		bKlink = (ImageButton) findViewById(R.id.imageButton1);
		bRS = (ImageButton) findViewById(R.id.imageButton2);
		bApotik = (ImageButton) findViewById(R.id.imageButton3);
		bTips = (ImageButton) findViewById(R.id.imageButton4);

		bKlink.setOnClickListener(this);
		bRS.setOnClickListener(this);
		bApotik.setOnClickListener(this);
		bTips.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.imageButton1:
			var.setSelected(0);
			startActivity(new Intent(this, TabHostActivity.class));
			break;
		case R.id.imageButton2:
			var.setSelected(1);
			startActivity(new Intent(this, TabHostActivity.class));

			break;
		case R.id.imageButton3:
			var.setSelected(2);
			startActivity(new Intent(this, TabHostActivity.class));

			break;
		case R.id.imageButton4:
			var.setSelected(3);
			startActivity(new Intent(this, TipsActivity.class));

			break;

		}
	}

}
