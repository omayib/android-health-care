package com.ahc.app;

import java.io.IOException;
import java.util.ArrayList;

import com.ahc.app.helper.AHCBaseAdapter;
import com.ahc.app.helper.DataBaseHelper;
import com.ahc.app.helper.EntitasTempat;
import com.ahc.app.helper.GlobalVar;

import android.app.Activity;
import android.app.Dialog;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ListAHC_Activity extends Activity {
	private DataBaseHelper db;
	ListView lv;
	GlobalVar var;
	EntitasTempat entitastempat;
	ArrayList<EntitasTempat> tempat = new ArrayList<EntitasTempat>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		var = ((GlobalVar) getApplicationContext());
		db = new DataBaseHelper(this);
		try {
			db.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}

		try {
			db.openDataBase();
		} catch (SQLException sqle) {
			throw sqle;
		}

		lv = (ListView) findViewById(R.id.listAHC);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		ArrayList<ArrayList<Object>> data = db.AmbilKategori(var.getSelected());
		Log.i("data", String.valueOf(data.size()));
		if (data.size() > 0) {
			for (int i = 0; i < data.size(); i++) {
				ArrayList<Object> b = data.get(i);
				entitastempat = new EntitasTempat();
				entitastempat
						.setIDtempat(Integer.parseInt(b.get(0).toString()));
				entitastempat.setNamatempat(b.get(1).toString());
				entitastempat.setAlamattempat(b.get(2).toString());
				entitastempat.setNope(b.get(3).toString());
				tempat.add(entitastempat);
			}
			AHCBaseAdapter ba = new AHCBaseAdapter(this, tempat);
			lv.setAdapter(ba);
			lv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					ShowDetil(arg2);
				}
			});
		} else {
			Toast.makeText(this, "no data", Toast.LENGTH_SHORT).show();

		}
	}

	protected void ShowDetil(int arg2) {
		// TODO Auto-generated method stub
		final Dialog d = new Dialog(this);
		d.setTitle("Info Detail");

		d.setContentView(R.layout.custom_dialog);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(d.getWindow().getAttributes());
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = LayoutParams.WRAP_CONTENT;

		d.getWindow().setAttributes(lp);
		Button bok = (Button) d.findViewById(R.id.bOk);
		ImageView im = (ImageView) d.findViewById(R.id.icon);
		im.setVisibility(View.GONE);
		TextView tnama = (TextView) d.findViewById(R.id.nama_txt);
		TextView taddr = (TextView) d.findViewById(R.id.alamat_txt);
		TextView tphone = (TextView) d.findViewById(R.id.phone_txt);

		im.setImageResource(R.drawable.apotek32);
		tnama.setText(" : " + tempat.get(arg2).getNamatempat());
		taddr.setText(" : " + tempat.get(arg2).getAlamattempat());
		tphone.setText(" : " + tempat.get(arg2).getNope());

		// Intent iCall = new Intent(Intent.ACTION_CALL);
		// iCall.setData(Uri.parse("tel:" + string3));
		// startActivity(iCall);
		bok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				d.dismiss();
			}
		});
		d.show();

	}

}
