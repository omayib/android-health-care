package com.ahc.app.helper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelper extends SQLiteOpenHelper {

	// The Android's default system path of your application database.
	private static String DB_PATH = "/data/data/com.ahc.app/databases/";
	private static String DB_NAME = "HealthCare.sqlite3";
	// private static String DB_NAME = "db";
	private SQLiteDatabase myDataBase;

	private final Context myContext;

	private static final String TABLE_KLINIK = "tb_klinik";
	private static final String TABLE_RS = "tb_rumahsakit";
	private static final String TABLE_APOTEK = "tb_apotek";
	private static final String ROW_ID = "_id";
	private static final String ROW_NAMA = "nama";
	private static final String ROW_ALAMAT = "alamat";
	private static final String ROW_NOPE = "nope";
	private static final String ROW_LAT = "lat";
	private static final String ROW_LON = "lon";
	Cursor curkat;

	/**
	 * Constructor Takes and keeps a reference of the passed context in order to
	 * access to the application assets and resources.
	 * 
	 * @param context
	 */
	public DataBaseHelper(Context context) {

		super(context, DB_NAME, null, 1);
		this.myContext = context;
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {

		boolean dbExist = checkDataBase();

		if (dbExist) {
			// do nothing - database already exist

		} else {

			// By calling this method and empty database will be created into
			// the default system path
			// of your application so we are gonna be able to overwrite that
			// database with our database.

			this.getReadableDatabase();
			this.close();
			try {

				copyDataBase();

			} catch (IOException e) {
				e.printStackTrace();

				throw new Error("Error copying database");

			}
		}

	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase() {

		SQLiteDatabase checkDB = null;

		try {
			// String FOLDER_PATH = Environment.getExternalStorageDirectory()
			// .getAbsolutePath() + File.separator + "Pubkey";
			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READONLY);
			Log.d("checkdb", myContext.getAssets().toString());
		} catch (SQLiteException e) {
			Log.d("checkdb", "tidak ada");
			// database does't exist yet.

		}

		if (checkDB != null) {
			Log.d("checkdb", "close");
			checkDB.close();

		}

		return checkDB != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException {

		// Open your local db as the input stream
		InputStream myInput = myContext.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outFileName = DB_PATH + DB_NAME;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	public void openDataBase() throws SQLException {

		// Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READWRITE);

	}

	@Override
	public synchronized void close() {

		if (myDataBase != null)
			myDataBase.close();

		super.close();

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	// Add your public helper methods to access and get content from the
	// database.
	// You could return cursors by doing "return myDataBase.query(....)" so it'd
	// be easy
	// to you to create adapters for your views.
	// ------------------------------------------------------ //
	public ArrayList<ArrayList<Object>> AmbilKategori(int kat) {

		ArrayList<ArrayList<Object>> dataArray = new ArrayList<ArrayList<Object>>();

		try {
			switch (kat) {
			case 0:// rumahsakit

				curkat = myDataBase.rawQuery("SELECT * FROM " + TABLE_RS, null);
				break;

			case 1:// klinik
				curkat = myDataBase.rawQuery("SELECT * FROM " + TABLE_KLINIK,
						null);
				break;
			case 2:// apotek
				curkat = myDataBase.rawQuery("SELECT * FROM " + TABLE_APOTEK,
						null);
				break;
			case 3:// tips
				curkat = myDataBase.rawQuery("SELECT * FROM " + TABLE_KLINIK,
						null);
				break;
			}
			curkat.getCount();
			curkat.moveToFirst();
			if (!curkat.isAfterLast()) {
				do {
					ArrayList<Object> dataList = new ArrayList<Object>();
					dataList.add(curkat.getInt(0));
					dataList.add(curkat.getString(1));
					dataList.add(curkat.getString(2));
					dataList.add(curkat.getString(3));
					dataList.add(curkat.getString(4));
					dataList.add(curkat.getString(5));
					// Log.d("nama", cur.getString(2));

					dataArray.add(dataList);

				} while (curkat.moveToNext());

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("DEBE ERROR", e.toString());

		}
		return dataArray;
	}

	public ArrayList<ArrayList<Object>> ambilMuseumDetil(int rowId) {
		ArrayList<ArrayList<Object>> arrbaris = new ArrayList<ArrayList<Object>>();
		Cursor cursor;
		try {

			Log.i("TAG==>", String.valueOf(rowId));
			cursor = myDataBase
					.rawQuery(
							"SELECT m.rowid, m.nama, m.alamat, m.phone, "
									+ "m.deskripsi, m.pic, w.nama,m.latlng,m.jambuka,m.tarif FROM "
									+ "museum_tb m JOIN tb_wilayah w ON m.kode_wilayah=w.idwilayah WHERE "
									+ ROW_ID + " = " + rowId, null);
			// cursor = myDataBase.query(TABLE_NAME, new String[] { ROW_ID,
			// ROW_NAMA, ROW_LOKASI, ROW_LATLON }, ROW_ID + "=" + rowId,
			// null, null, null, null, null);
			cursor.moveToFirst();
			if (!cursor.isAfterLast()) {
				do {
					ArrayList<Object> data = new ArrayList<Object>();
					data.add(cursor.getInt(0));
					data.add(cursor.getString(1));
					data.add(cursor.getString(2));
					data.add(cursor.getString(3));
					data.add(cursor.getString(4));
					data.add(cursor.getString(5));
					data.add(cursor.getString(6));
					data.add(cursor.getString(7));
					data.add(cursor.getString(8));
					data.add(cursor.getString(9));
					// Log.i("TAG-", cursor.getString(0));
					// Log.i("TAG-", cursor.getString(1));
					// Log.i("TAG-", cursor.getString(2));
					// Log.i("TAG-", cursor.getString(3));
					// Log.i("TAG-", cursor.getString(4));
					// Log.i("TAG-", cursor.getString(5));
					// Log.i("TAG-", cursor.getString(6));
					arrbaris.add(data);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("error", e.toString());
		}

		return arrbaris;
	}
}